# debian backports

This repo contains currently one backport for the "bullseye" Debian Linux.


## pan_0.149-1~bpo11+1_amd64.deb 

This backport was built along instructions seen at [SimpleBackportCreation](https://wiki.debian.org/SimpleBackportCreation).
It is meant to supersede bullseye pan version 0.146-2, which appears to be affected by issues 
[#103](https://gitlab.gnome.org/GNOME/pan/-/issues/103) and [#114](https://gitlab.gnome.org/GNOME/pan/-/issues/114) 
("posting failure").

* Download: [pan_0.149-1~bpo11+1_amd64.deb](bin/pan_0.149-1~bpo11+1_amd64.deb) (1MB) (2022-01-04) 
  md5: 4cdb534e2809f15d1001feae1736d0ee  pan_0.149-1~bpo11+1_amd64.deb  
  sha1: f0df004019f02b0dce22f11609f776cc48a16b1c  pan_0.149-1~bpo11+1_amd64.deb  
  sha256: 383a1abdcd74c6212defa581265217f7f85340ab5ae91ed831c22e810d1321fd  pan_0.149-1~bpo11+1_amd64.deb

This is a private debian package. The original package's and Debian licensing applies.

~2022-01-11




